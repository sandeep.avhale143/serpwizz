import Vue from 'vue'

import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import KnobControl from 'vue-knob-control'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import routes from './Routes'
 
Vue.use(KnobControl)
Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)


Vue.use(VueRouter)



// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

const router = new VueRouter({
	routes: routes,
	mode : "history"
});

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
